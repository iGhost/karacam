<?php
set_time_limit(0);
define('SAVE_IMAGE_PATH', './screens/');

function get_link($cam_id)
{
	$h = curl_init('http://karakol-ski.kg/yowidget/return_link.php');
	curl_setopt($h, CURLOPT_POST, 1);
	curl_setopt($h, CURLOPT_POSTFIELDS, array('a'=>$cam_id));
	curl_setopt($h, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($h);
	curl_close($h);
	return $result;
}

function correct_link($link)
{
	$fullLink = explode(':', $link);
	return 'http://' . trim(array_pop($fullLink), '.');
}

function save_image($url, $dir=false)
{
	$images_dir = SAVE_IMAGE_PATH . DIRECTORY_SEPARATOR . date('Y-m-d');
	if(!is_dir($images_dir)) {
		mkdir($images_dir, 0777);
	}
	if($dir) {
		$images_dir .= DIRECTORY_SEPARATOR . $dir;
		if(!is_dir($images_dir)) {
			mkdir($images_dir, 0777);
		}
	}
	$path = realpath($images_dir) .'/'. basename($url);
	if (!file_exists($path)) {
		exec("wget -nv '$url' -O '$path' -o log.log");
		exec("mogrify -quality 78 '$path'");
	}
}

function process_image($cam_id, $dir=false)
{
	$link = get_link($cam_id);
	$link = correct_link($link);
//	echo $link."\n";
	save_image($link, $dir);
}

if(!is_dir(SAVE_IMAGE_PATH)) {
	mkdir(SAVE_IMAGE_PATH, 0777);
}

while(1) {
	process_image('IPCam450436');
	process_image('IPCam449570', 'cafe-slope');
	process_image('IPCam000003', 'scholar-lifter');
	sleep(20);
}
